# galaxie-vpn

## First step

Prepare everything automatically
``` shell
make prepare
```

Create a inventory file by exemple ``inventory_remote` filename, where the content is:
``` text
vpn_host_1_to_provision ansible_host=YOU_HOST_IP

vpn_host_1 ansible_host=YOU_HOST_IP

[to_provision]
vpn_host_1_to_provision

[to_provision:vars]
ansible_ssh_user=root
ansible_password=YOU_ROOT_PASSWORD

[vpn]
vpn_host_1

[vpn:vars]
ansible_ssh_user=caretaker
ansible_ssh_private_key_file=./secrets/caretaker/keys/id_ed25519
ansible_becomessh _method=sudo
```
You can use all alternative Ansible method's , the exemple is here for illustrat how to ...

Load the
``` shell
make deploy_bootstrap
```
On the remote host

```shell
apk update
apk add python3
```

