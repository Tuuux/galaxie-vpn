.PHONY: help install-python venv-create venv-requirements clean tests-gitlab-ci tests-shunit2 documentations
SHELL=/bin/bash

#!make
include .env
export $(shell sed 's/=.*//' .env)

help:
	@echo  'CleaningUp:'
	@echo  '  clean           - Remove every created directory and restart from scratch'
	@echo  ''
	@echo  'VirtualEnv:'
	@echo  '  prepare            - The easy way to prepareeverything with minimal effort
	@echo  '  venv-create        - Create virtual env directory with python venv module'
	@echo  '  venv-requirements  - Call venv-create and install or update packages:'
	@echo  '                       pip, wheel, setuptools and install what is list on'
	@echo  '                       requirements.txt file'
	@echo  ''

header:
	@echo  '***************************** GALAXIE VPN MAKEFILE *****************************'
	@echo  ''
	@echo  ''
	@echo  'GNU GENERAL PUBLIC LICENSE V3 OR LATER (GPLV3+)'
	@echo  "LOADER `uname -v`"
	@echo  'EXEC MAKE'
	@ echo "`free -t -b | grep -oP '\d+' | sed '1!d' | numfmt --to=iec --suffix=B --format %.2f` RAM SYSTEM"
	@ echo "`free -t -b | grep -oP '\d+' | sed '3!d' | numfmt --to=iec --suffix=B --format %.2f` FREE"
	@echo  'NO HOLOTAPE FOUND'
	@echo  'NO PLUGINS TO LOAD'
	@echo  ''

install-python:
	apt install -y python3 python3-pip python3-venv make

venv-create: header
	@echo "> VIRTUAL ENV CREATION"
	@test -d $(DEV_VENV_DIRECTORY) || cd $(DEV_DIRECTORY) && python3 -m venv $(DEV_VENV_NAME)

venv-control: venv-create 	## Control venv pip, wheel and setuptools installation/update
	@echo "> VIRTUAL ENV CONTROL"
	@${DEV_VENV_ACTIVATE} && pip3 install -U --no-cache-dir --quiet pip  wheel setuptools
	@${DEV_VENV_ACTIVATE} && pip3 install -U --no-cache-dir --quiet -r $(DEV_PYTHON_REQUIREMENTS_FILE)

create-directories:
	@${DEV_VENV_ACTIVATE} && ansible-playbook  -i inventory_local playbooks/prepare_directories.yml

gen-keys: create-directories
	@${DEV_VENV_ACTIVATE} && ansible-playbook  -i inventory_local playbooks/prepare_generate_caretaker_sshkeys.yml
	@${DEV_VENV_ACTIVATE} && ansible-playbook  -i inventory_local playbooks/prepare_link_secret_directory.yml


env:
	pip3 install -U --no-cache-dir --quiet pip wheel setuptools
	pip3 install -U --no-cache-dir --quiet -r $(DEV_PYTHON_REQUIREMENTS_FILE)
	ansible-galaxy collection install -fr requirements.yml

prepare: venv-control gen-keys
	@ ${DEV_VENV_ACTIVATE} && pip3 install -U -q pip setuptools wheel
	@ ${DEV_VENV_ACTIVATE} && pip3 install -U -q pip -r $(DEV_PYTHON_REQUIREMENTS_FILE)

clean:
	rm -rf ./venv
	rm -rf ~/.cache/pip


documentations: prepare
	@ ${DEV_VENV_ACTIVATE} &&\
 	pip install -U --no-cache-dir -q install mkdocs &&\
 	mkdocs build -d docs/site

deploy_bootstrap: prepare
	@${DEV_VENV_ACTIVATE} && ansible-playbook  -i inventory_remote playbooks/deploy_bootstrap.yml

deploy_system: deploy_bootstrap
	@${DEV_VENV_ACTIVATE} && ansible-playbook  -i inventory_remote playbooks/deploy_system.yml

deploy_wireguard:
	@${DEV_VENV_ACTIVATE} && ansible-playbook  -i inventory_remote playbooks/deploy_wireguard.yml